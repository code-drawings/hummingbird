var cursorX = 0;
var cursorY = 0;


var colors = ["#104740", "#8E0017", "#160D00", "#7E914E", "#9B8431"];

document.addEventListener('mousemove', function(e){
    cursorX = e.pageX;
    cursorY = e.pageY;
});

document.addEventListener('DOMContentLoaded', function() {

    var canvas = document.getElementById('c');
    var cs = getComputedStyle(canvas);
    canvas.width = parseInt(cs.width);
    canvas.height = parseInt(cs.height);
    var ctx = canvas.getContext('2d');

    window.requestAnimationFrame(function() {draw(ctx, canvas)});

});

var lastXY = [0, 0];

function draw(ctx, canvas) {
    var time = Date.now();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    lastXY = drawHummingbird(ctx, ...approachMouse(cursorX, cursorY, ...lastXY), time);
    window.requestAnimationFrame(function() {draw(ctx, canvas)});
}

function approachMouse(mx, my, bx, by) {
    var dvx = (mx - bx) / 20;
    var dvy =  (my - by) / 20;
    return [dvx+bx, dvy+by];
}

var beakTip = (x, y) => [x, y];
var beakNortheast = (x, y) => [x+17, y];
var beakSoutheast = (x, y) => [x+20, y+5];
var bodySoutheast = (x, y) => [x+57, y+30];
var bodyNortheast = (x, y) => [x+17+5, y-5];
var wingSouthwest = (x, y) => [x+(77/2), y+(35/2)]
var wingSoutheasT = (x, y, t) => [x+(77/2)+5-20*(Math.sin(t/20)+1), y+(35/2)-15+30*Math.sin(t/20)]
var wingNortheasT = (x, y, t) => [x+(77/2)+20-20*(Math.sin(t/20)+1), y+(35/2)-30+30*Math.sin(t/20)]


function drawHummingbird(ctx, x, y, t) {
    ctx.fillStyle = colors[0];

    ctx.beginPath();
    ctx.moveTo(...beakTip(x, y));
    ctx.lineTo(...beakNortheast(x, y));
    ctx.lineTo(...beakSoutheast(x, y));
    ctx.fill();

    ctx.fillStyle = colors[1];
    ctx.beginPath();
    ctx.moveTo(...beakNortheast(x, y));
    ctx.lineTo(...beakSoutheast(x, y));
    ctx.lineTo(...bodySoutheast(x, y));
    ctx.lineTo(...bodyNortheast(x, y));
    ctx.fill();

    ctx.fillStyle = colors[3];
    ctx.beginPath();
    ctx.moveTo(...wingSouthwest(x, y));
    ctx.lineTo(...wingSoutheasT(x, y, t));
    ctx.lineTo(...wingNortheasT(x, y, t));
    ctx.fill();

    return [x, y];

}

function popRand(arr) {
    return arr.splice(Math.floor(Math.random()*arr.length), 1);
}